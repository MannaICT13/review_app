//
//  ViewController.swift
//  review_app
//
//  Created by Md Khaled Hasan Manna on 7/12/20.
//

import UIKit
import StoreKit
class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Review", style: .done, target: self, action: #selector(reviewApp(_ :)))
        
        
        
    }
    @objc func reviewApp(_ sender : UIBarButtonItem){
        
        if let scene = UIApplication.shared.connectedScenes.first as? UIWindowScene{
            SKStoreReviewController.requestReview(in:scene)
            
        }
        
    }


}

